package isi.tn.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import isi.tn.entities.Billet;

@Repository
public interface BilletRepository extends JpaRepository<Billet,Long> {

}

