package isi.tn.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isi.tn.entities.Billet;
import isi.tn.entities.MatchQuatar;
import isi.tn.entities.Spectateur;
import isi.tn.repository.BilletRepository;
import isi.tn.repository.MatchRepository;
import isi.tn.repository.SpectateurRepository;


@RestController
@RequestMapping("/api" )
public class SpectateurController {
	
	@Autowired	
   SpectateurRepository srepo;	
	
	@GetMapping("/allspectateur")
	public List<Spectateur> getAllSpectateurs() {
		List<Spectateur> pro = srepo.findAll();
        return pro;
	    
	}	
	@PostMapping("/addSpectateur")
	public Spectateur createSpectateur(@Valid @RequestBody Spectateur pro) {
	    return srepo.save(pro);
	}	
	@DeleteMapping("/deleteS/{id}")

	public void delete(@PathVariable Long id) {
		 srepo.deleteById(id);
		
		 
	}
	@PutMapping("/updateS/{id}")
	public Spectateur updateUser( @PathVariable (value = "id") Long id, @Valid @RequestBody Spectateur userDetails) {		
		 Spectateur user = srepo.findById(id).orElseThrow(null);    	   
	    user.setNom(userDetails.getNom());
	    user.setPrenom(userDetails.getPrenom());	     
	    return srepo.save(user);
	}
}

