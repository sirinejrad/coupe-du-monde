package isi.tn.controller;



import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isi.tn.entities.Billet;
import isi.tn.entities.MatchQuatar;
import isi.tn.repository.BilletRepository;
import isi.tn.repository.MatchRepository;
@RestController
@RequestMapping("/api")
public class BilletController {

		@Autowired 
		BilletRepository prepo;
		
		@GetMapping("/allbillet")
		public List<Billet> getAllBillet() {
			List<Billet> pro = prepo.findAll();
	        return pro;
		    
		}
		@PostMapping("/addBillet")
		public Billet createMatch(@Valid @RequestBody Billet pro) {
		    return prepo.save(pro);
		}
		
	
		@DeleteMapping("/deleteB/{id}")
		
		public void delete(@PathVariable Long id) {
			 prepo.deleteById(id);
			
			 
		}
		
	

}
