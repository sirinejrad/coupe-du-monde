package isi.tn.controller;



import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import isi.tn.entities.MatchQuatar;
import isi.tn.repository.MatchRepository;
@RestController
@RequestMapping("/api")
public class MatchController {
	
	@Autowired 
	MatchRepository prepo;
	
	@GetMapping("/matchs")
	public List<MatchQuatar> getAllMatch() {
		List<MatchQuatar> pro = prepo.findAll();
        return pro;
	    
	}
	@PostMapping("/addmatch")
	public MatchQuatar createMatch(@Valid @RequestBody MatchQuatar pro) {
	    return prepo.save(pro);
	}
/*
	@DeleteMapping("/delete/{matchid}")
	 public void deleteMatchById(Long matchid)
    {
        Optional<MatchQuatar> m = prepo.findById(matchid);
         
        if(m.isPresent())
        {
            prepo.deleteById(matchid);
        } 
    }
	*/
	
	
	@DeleteMapping("/delete/{id}")
	
	public void delete(@PathVariable Long id) {
		 prepo.deleteById(id);
		
		 
	}
	
	@PutMapping("/updateM/{id}")
	public MatchQuatar updateUser( @PathVariable (value = "id") Long id, @Valid @RequestBody MatchQuatar userDetails) {		
		 MatchQuatar user = prepo.findById(id).orElseThrow(null);    	   
	    user.setNomMatch(userDetails.getNomMatch());
	    user.setHoraire(userDetails.getHoraire());	     
	    return prepo.save(user);
	}

}
