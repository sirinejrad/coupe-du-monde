package isi.tn.entities;


import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class MatchQuatar implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="Matchid")
	private Long id;	    
	private String nomMatch;	    
	private String horaire;
	
	//@OneToMany(mappedBy="porject",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@OneToMany(mappedBy="mtch",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonIgnore
	//@JsonBackReference
	
	Set<Billet> billets ; 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomMatch() {
		return nomMatch;
	}

	public void setNomMatch(String nomMatch) {
		this.nomMatch = nomMatch;
	}

	public String getHoraire() {
		return horaire;
	}

	public void setHoraire(String horaire) {
		this.horaire = horaire;
	}

	public Set<Billet> getBillets() {
		return billets;
	}

	public void setBillets(Set<Billet> billets) {
		this.billets = billets;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MatchQuatar(Long id, String nomMatch, String horaire, Set<Billet> billets) {
		super();
		this.id = id;
		this.nomMatch = nomMatch;
		this.horaire = horaire;
		this.billets = billets;
	}

	public MatchQuatar() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//@ManyToMany (cascade = CascadeType.ALL,fetch = FetchType.LAZY)

	

	
	
	

}
