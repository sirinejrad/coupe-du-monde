package isi.tn.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity 
@Table(name = "product")
public class Product{
  private String name;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id")
    private Company company;
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product(String name, Company company, Long id) {
		super();
		this.name = name;
		this.company = company;
		this.id = id;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

}
