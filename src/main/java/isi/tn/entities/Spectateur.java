package isi.tn.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Spectateur implements Serializable  {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="spectateurid")
	private Long id;
	private String nom;	
	private String prenom;
	@OneToOne
	@JsonIgnore
	private Billet billets;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Billet getBillet() {
		return billets;
	}

	public void setBillet(Billet billets) {
		this.billets = billets;
	}

	public Spectateur(Long id, String nom, String prenom, Billet billets) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.billets = billets;
	}
	public Spectateur() {
		super();
	}
	
}