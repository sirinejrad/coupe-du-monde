package isi.tn.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Billet implements Serializable{

private static final long serialVersionUID = 1L;
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)  
@Column(name="billetid")
private Long id;
@ManyToOne//(cascade = CascadeType.ALL,fetch = FetchType.LAZY)

private MatchQuatar mtch; 

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public MatchQuatar getPorject() {
	return mtch;
}



public void setPorject(MatchQuatar mtch) {
	this.mtch = mtch;
}


public Billet(Long id, MatchQuatar mtch) {
	super();
	this.id = id;
	this.mtch = mtch;
}

public Billet() {
	super();
}




}
