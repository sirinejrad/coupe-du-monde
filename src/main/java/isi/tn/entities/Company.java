package isi.tn.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
@Entity
@Table(name = "company")
public class Company{
    private String name;
    
    @Autowired
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Product> products ; 	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long  id ;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Company(String name, List<Product> products, Long id) {
		super();
		this.name = name;
		this.products = products;
		this.id = id;
	}
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
